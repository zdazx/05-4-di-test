package com.twuc.webApp.web;

import com.twuc.webApp.service.GameLevelService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class MazeController {
    private final GameLevelService gameLevelService;

    public MazeController(GameLevelService gameLevelService) {
        this.gameLevelService = gameLevelService;
    }

    @GetMapping("/buffered-mazes/{type}")
    public ResponseEntity<byte[]> getMaze(
        @RequestParam(required = false, defaultValue = "10") int width,
        @RequestParam(required = false, defaultValue = "10") int height,
        @PathVariable String type) throws IOException {

        byte[] bytes = gameLevelService.renderMaze(
            width,
            height,
            type
        );

        return ResponseEntity
            .ok()
            .contentType(MediaType.IMAGE_PNG)
            .body(bytes);
    }

    @GetMapping("/mazes/{type}")
    @ResponseStatus(HttpStatus.OK)
    public void getMaze2(
        HttpServletResponse response,
        @RequestParam(required = false, defaultValue = "10") int width,
        @RequestParam(required = false, defaultValue = "10") int height,
        @PathVariable String type) throws IOException {

        response.setContentType(MediaType.IMAGE_PNG_VALUE);

        gameLevelService.renderMaze(
            response.getOutputStream(),
            width,
            height,
            type
        );
    }
}
